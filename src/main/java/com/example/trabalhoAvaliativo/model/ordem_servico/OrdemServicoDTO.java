package com.example.trabalhoAvaliativo.model.ordem_servico;

import java.util.Date;

public interface OrdemServicoDTO {
    Long getId();
    String getMarca();
    String getModelo();
    String getTipo();
    String getProblema();
    String getStatus();
    String getData_entrada();
    String getData_saida();
    double getTotal();
    boolean getAtivo();
}

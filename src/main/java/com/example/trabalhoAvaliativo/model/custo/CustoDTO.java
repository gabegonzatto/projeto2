package com.example.trabalhoAvaliativo.model.custo;

public interface CustoDTO {
    Long getId();

    double getValor();
}

package com.example.trabalhoAvaliativo.model.produto;

public interface ProdutoDTO {
    Long getId();
    String getNome();
}

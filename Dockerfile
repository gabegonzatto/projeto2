FROM openjdk:17

ADD ./target/projeto.jar projeto.jar

ENTRYPOINT ["java", "-jar","projeto.jar"]

EXPOSE 8080
